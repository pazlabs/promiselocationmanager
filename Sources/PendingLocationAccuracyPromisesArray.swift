//
//  PendingLocationAccuracyPromisesArray.swift
//  PromiseLocationManager
//
//  Created by Pantelis Zirinis on 29/03/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation
import Promises
import CoreLocation

class LocationAccuracyPromise {
    let accuracy: CLLocationAccuracy
    let promise: LocationPromise
    
    init(accuracy: CLLocationAccuracy, promise: LocationPromise) {
        self.accuracy = accuracy
        self.promise = promise
    }
}

/// Used to protect the promises array from simultaneous access
class PendingLocationAccuracyPromisesArray {
    
    enum Error: Swift.Error {
        case promiseArrayDeallocated
    }
    
    private lazy var updatesQueue: DispatchQueue = DispatchQueue(label: RandomLockName("PendingPromisesArray"), attributes: .concurrent)
    private var promises = [LocationAccuracyPromise]()
    
    public func addPromise(_ promise: LocationAccuracyPromise) -> Promise<Void> {
        return Promise<Void>(on: self.updatesQueue) { [weak self] in
            guard let strongSelf = self else {
                promise.promise.reject(Error.promiseArrayDeallocated)
                return
            }
            strongSelf.promises.append(promise)
        }
    }
    
    public func fulfillPromises(location: CLLocation) -> Promise<Void> {
        return Promise<[LocationAccuracyPromise]>(on: self.updatesQueue) { [weak self] in
            guard let promises = self?.promises else {
                return [LocationAccuracyPromise]()
            }
            let fullfilPromises = promises.filter({ (locationAccuracyPromise) -> Bool in
                return location.horizontalAccuracy <= locationAccuracyPromise.accuracy
            })
            let notFullfilPromises = promises.filter({ (locationAccuracyPromise) -> Bool in
                return !(location.horizontalAccuracy <= locationAccuracyPromise.accuracy)
            })
            self?.promises = notFullfilPromises
            return fullfilPromises
        }.then({ (promises) in
            for promise in promises {
                promise.promise.fulfill(location)
            }
            return Promise<Void> { }
        })
    }
    
    public func failPromises(_ error: Swift.Error) -> Promise<Void> {
        return Promise<[LocationAccuracyPromise]>(on: self.updatesQueue) { [weak self] in
            guard let promises = self?.promises else {
                return [LocationAccuracyPromise]()
            }
            self?.promises.removeAll()
            return promises
        }.then { (promises) in
            for promise in promises {
                promise.promise.reject(error)
            }
            return Promise<Void> { }
        }
    }
    
    public func removePromise(_ promise: LocationAccuracyPromise) -> Promise<Void> {
        return Promise<Void>(on: self.updatesQueue) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.promises = strongSelf.promises.filter() { $0 !== promise }
            return
        }
    }
    
    public func countPromises() -> Promise<Int> {
        return Promise<Int>(on: self.updatesQueue) { [weak self] in
            return self?.promises.count ?? 0
        }
    }
    
    public func desiredAccuracy() -> Promise<CLLocationAccuracy> {
        return Promise<CLLocationAccuracy>(on: self.updatesQueue) { [weak self] in
            var accuracy = kCLLocationAccuracyThreeKilometers
            if let promises = self?.promises {
                for promise in promises {
                    if promise.accuracy < accuracy {
                        accuracy = promise.accuracy
                    }
                }
            }
            return accuracy
        }
    }
    
}


