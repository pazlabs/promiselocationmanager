//
//  PendingPromisesArray.swift
//  PromiseLocationManager
//
//  Created by Pantelis Zirinis on 27/03/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation
import Promises

func RandomLockName(_ prefix: String) -> String {
    let random = Int(arc4random_uniform(10000))
    return "\(prefix).\(random)"
}

class PendingPromisesArray<Element> {
    
    enum Error: Swift.Error {
        case promiseArrayDeallocated
    }
    
    private lazy var updatesQueue: DispatchQueue = DispatchQueue(label: RandomLockName("PendingPromisesArray"), attributes: .concurrent)
    private var promises = [Promise<Element>]()
    
    public func addPromise(_ promise: Promise<Element>) -> Promise<Void> {
        return Promise<Void>(on: self.updatesQueue) { [weak self] in
            guard let strongSelf = self else {
                promise.reject(Error.promiseArrayDeallocated)
                return
            }
            strongSelf.promises.append(promise)
        }
    }
    
    public func fulfillPromises(element: Element) -> Promise<Void> {
        return Promise<[Promise<Element>]>(on: self.updatesQueue) { [weak self] in
            guard let promises = self?.promises else {
                return [Promise<Element>]()
            }
            self?.promises.removeAll()
            return promises
        }.then({ (promises) in
            for promise in promises {
                promise.fulfill(element)
            }
            return Promise<Void> { }
        })
    }
    
    public func failPromises(_ error: Swift.Error) -> Promise<Void> {
        return Promise<[Promise<Element>]>(on: self.updatesQueue) { [weak self] in
            guard let promises = self?.promises else {
                return [Promise<Element>]()
            }
            self?.promises.removeAll()
            return promises
        }.then { (promises) in
            for promise in promises {
                promise.reject(error)
            }
            return Promise<Void> { }
        }
    }
    
    public func removePromise(_ promise: Promise<Element>) -> Promise<Void> {
        return Promise<Void>(on: self.updatesQueue) { [weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.promises = strongSelf.promises.filter() { $0 !== promise }
            return
        }
    }

}

