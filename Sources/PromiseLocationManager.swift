//
//  PromiseLocationManager.swift
//  PromiseLocationManager
//
//  Created by Pantelis Zirinis on 27/03/2018.
//  Copyright © 2018 Pantelis Zirinis. All rights reserved.
//

import Foundation
import CoreLocation
import Promises

// TODO: Add desired accuracy and timeout.
// After timeout if accuracy is not met, return whatever we got
// Accuracy and shoud be added on the location request and not framework wide

public typealias LocationPromise = Promise<CLLocation>

public class PromiseLocationManager: NSObject {
    
    public enum Error: Swift.Error {
        case locationServicesDisabled
        case locationNotEnabledOnInfoPlist
        case restrictedAccess
        case deniedAccess
    }
    
    private var authRequestActive = false
    
    public static var shared = PromiseLocationManager()
    
    lazy private var _locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        let code = CLLocationManager.authorizationStatus()
        if code == CLAuthorizationStatus.notDetermined {
            if let _ = Bundle.main.object(forInfoDictionaryKey: "NSLocationAlwaysUsageDescription") as? String {
                self.authRequestActive = true
                locationManager.requestAlwaysAuthorization()
            } else if let _ = Bundle.main.object(forInfoDictionaryKey: "NSLocationWhenInUseUsageDescription") as? String {
                self.authRequestActive = true
                locationManager.requestWhenInUseAuthorization()
            } else {
                self.locationManagerPromises.failPromises(Error.locationNotEnabledOnInfoPlist).then {
                    print("!!!WARNING:!!!\nInfo.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
                }
            }
        }
        return locationManager
    }()
    
    private var locationManagerPromises = PendingPromisesArray<CLLocationManager>()
    
    var locationManager: Promise<CLLocationManager> {
        
        guard CLLocationManager.locationServicesEnabled() else {
            return Promise<CLLocationManager> {
                throw Error.locationServicesDisabled
            }
        }
        // Lazy load location manager if needed
        let _ = self._locationManager
        if self.authRequestActive {
            let promise = Promise<CLLocationManager>.pending()
            self.locationManagerPromises.addPromise(promise).then {
            }
            return promise
        } else {
            return Promise<CLLocationManager> {
                return self._locationManager
            }
        }
    }
    
    private var locationPromises = PendingLocationAccuracyPromisesArray()
    
    public func location(_ accuracy: CLLocationAccuracy = kCLLocationAccuracyThreeKilometers, maxCachedTime: TimeInterval = 60) -> LocationPromise {
        if let lastLocation = self.lastLocation, -lastLocation.timestamp.timeIntervalSinceNow < maxCachedTime, lastLocation.horizontalAccuracy <= accuracy {
            return LocationPromise {
                return lastLocation
            }
        }
        switch self.status {
        case .denied:
            return LocationPromise { throw Error.deniedAccess }
        case .restricted:
            return LocationPromise { throw Error.restrictedAccess }
        default:
            break
        }
        let promise = LocationPromise.pending()
        let accuracyPromise = LocationAccuracyPromise(accuracy: accuracy, promise: promise)
        return self.locationPromises.addPromise(accuracyPromise).then(updateLocationManager).then {
            return promise
        }
    }
    
    private (set) var status : CLAuthorizationStatus = .notDetermined {
        didSet {
            guard oldValue != self.status else {
                return
            }
            switch self.status {
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationManagerPromises.fulfillPromises(element: self._locationManager).then {}
            case .denied:
                self.locationManagerPromises.failPromises(Error.deniedAccess).then { }
                self.locationPromises.failPromises(Error.deniedAccess).then { }
            case .restricted:
                self.locationManagerPromises.failPromises(Error.restrictedAccess).then { }
                self.locationPromises.failPromises(Error.restrictedAccess).then { }
            case .notDetermined:
                print("Not Determined shoud not be reached here!!")
            }
            self.authRequestActive = false
        }
    }
    
    func updateLocationManager() -> Promise<Void> {
        return all(self.locationManager, self.locationPromises.countPromises()).then { (locationManager, count) -> Void in
            if count == 0 {
                locationManager.stopUpdatingLocation()
            } else {
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    private var lastLocation: CLLocation?
    
}

extension PromiseLocationManager: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        self.status = status
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locationPromises.failPromises(error).always {
            self.updateLocationManager().then {}
        }
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let arrayOfLocation = locations as Array
        guard let currentLocation = arrayOfLocation.last else {
            print("No Location Found")
            return
        }
        self.lastLocation = currentLocation
        self.locationPromises.fulfillPromises(location: currentLocation).always { self.updateLocationManager().then {} }
    }
    
}

